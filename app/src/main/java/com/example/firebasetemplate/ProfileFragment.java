package com.example.firebasetemplate;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.FragmentProfileBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;

import java.util.UUID;


public class ProfileFragment extends AppFragment {
    private FragmentProfileBinding binding;
    private Uri uriNewImage;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentProfileBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Glide.with(this).load(auth.getCurrentUser().getPhotoUrl()).into(binding.profileImage);
        binding.profileName.setText(auth.getCurrentUser().getDisplayName());

        binding.editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.profileName.setAlpha(0);
                binding.profileAboutMe.setAlpha(0);
                binding.profileImage.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
                binding.editProfile.setAlpha(0);
                binding.profileName.setEnabled(false);
                binding.profileAboutMe.setEnabled(false);
                binding.editProfile.setEnabled(false);

                binding.saveProfile.setAlpha(1);
                binding.newName.setAlpha(1);
                binding.aboutYou.setAlpha(1);
                binding.cancel.setAlpha(1);

                binding.profileImage.setOnClickListener(var -> galeria.launch("image/*"));
                appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri -> {
                    Glide.with(ProfileFragment.this).load(uri).into(binding.profileImage);
                    uriNewImage = uri;
                });

                binding.saveProfile.setOnClickListener(update -> {
                    FirebaseStorage.getInstance()
                            .getReference("/images/" + UUID.randomUUID() + ".jpg")
                            .putFile(uriNewImage)
                            .continueWithTask(update2 -> update2.getResult().getStorage().getDownloadUrl())
                            .addOnSuccessListener(urlDescarga -> {

                            })
                });

                binding.cancel.setOnClickListener(cancel -> {
                    binding.profileName.setAlpha(1);
                    binding.profileAboutMe.setAlpha(1);
                    binding.editProfile.setAlpha(1);
                    Glide.with(ProfileFragment.this).load(auth.getCurrentUser().getPhotoUrl()).into(binding.profileImage);
                    binding.profileName.setText(auth.getCurrentUser().getDisplayName());
                    binding.saveProfile.setAlpha(0);
                    binding.newName.setAlpha(0);
                    binding.aboutYou.setAlpha(0);
                    binding.cancel.setAlpha(0);
                });
            }
        });
    }
    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        appViewModel.setUriImagenSeleccionada(uri);
    });
}